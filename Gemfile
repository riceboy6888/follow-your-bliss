source 'https://rubygems.org'

gem 'rails', '3.2.18'

# Database
gem 'mysql2'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass', '~> 3.2.19'
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'compass-rails', '~> 2.0.0'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end

gem 'jquery-rails', '~> 3.0.0'

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
gem 'unicorn'

# Deploy with Capistrano
gem 'capistrano', '~> 2.15.5'

# Patch for Capistrano
gem 'net-ssh', '~> 2.7.0'

# To use debugger
gem 'debugger'

# Deface plugins
gem 'diffy', '~> 3.0.4'

#Refinery version 2.1
gem 'refinerycms', :git => 'https://github.com/riceboy6888/refinerycms.git', :branch => '2-0-spree' do
  gem 'refinerycms-core'
  gem 'refinerycms-dashboard'
  gem 'refinerycms-images'
  gem 'refinerycms-pages'
  gem 'refinerycms-resources'
end

gem 'refinerycms-menus',      :git => 'git://github.com/pylonweb/refinerycms-menus.git',           :branch => 'support-2.1'
gem 'refinerycms-inquiries',  :git => 'https://github.com/refinery/refinerycms-inquiries.git',     :branch => '2-1-stable'
gem 'refinerycms-copywriting'
gem 'refinerycms-blog',       :git => 'https://github.com/riceboy6888/refinerycms-blog.git',       :branch => '2-0-spree'
gem 'refinerycms-pc_banners', :git => 'https://github.com/riceboy6888/refinerycms-pc_banners.git', :branch => '2-1-stable'

# Spree version 2.0
gem 'spree', '2.0.11'
gem 'spree_gateway',     :git => 'https://github.com/spree/spree_gateway.git',     :branch => '2-0-stable'
gem 'spree_auth_devise', :git => 'https://github.com/spree/spree_auth_devise.git', :branch => '2-0-stable'

# Testing environement gems
group :development, :test do
  gem 'spring'
  gem 'rspec-rails', '~> 2.14.2'
  gem 'shoulda-matchers', '~> 2.6.2'
  gem 'capybara', '~> 2.4.1'
  gem 'launchy', '~> 2.4.2'
  gem 'selenium-webdriver', '~> 2.42.0'
  gem 'factory_girl_rails', '~> 4.4.1'
  gem 'rb-fsevent', '~> 0.9.4'
  gem 'guard-rspec', '~> 4.3.1'
  gem 'guard-spork', '~> 1.5.1'
  gem 'fuubar', '~> 1.3.3'
  gem 'terminal-notifier-guard', '~> 1.5.3'
  gem 'database_cleaner', '~> 1.3.0'
end
