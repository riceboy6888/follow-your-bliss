class CreateSpreeRefineryUserPlugins < ActiveRecord::Migration
  def change
    create_table :spree_refinery_user_plugins do |t|
      t.integer :user_id
      t.string :name
      t.integer :position

      t.timestamps
    end
    
    add_index :spree_refinery_user_plugins, :name
    add_index :spree_refinery_user_plugins, [:user_id, :name], :unique => true
  end
end
