require "spec_helper"

module Refinery
  describe "Blog::Posts" do

    let(:home_page)  { FactoryGirl.create(:home_page) }
    let(:about_page) { FactoryGirl.create(:about_page) }
    let(:blog_page) { FactoryGirl.create(:blog_page) }
    let!(:blog_post) do
      Globalize.with_locale(:en) { FactoryGirl.create(:blog_post, :title => "Refinery CMS blog post") }
    end

    before do
      # Stub the menu pages we're expecting
      Page.stub(:fast_menu).and_return([home_page, about_page, blog_page])
      visit "/"
    end

    describe "blog page" do
      before do
        click_link blog_page.title
      end

      it "should display" do
        # in #header
        should_have_selector_within("#title-bar", "#header")
        should_have_selector_within("#breadcrumb", "#title-bar")

        # in #body
        should_have_selector_within("#sidebar", "#wrapper")
        should_have_selector_within("#blog_posts_list", "main")
        page.should have_content(blog_post.title)
      end
      
      it "should show links in #breadcrumbs" do
        within("#breadcrumb") do
          page.should have_link(home_page.title)
          page.should have_content(blog_page.title)
          click_link home_page.title
        end
        should_not_have_selector_within("#title-bar", "#header")          
      end
    end

  end
end