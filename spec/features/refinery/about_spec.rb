# encoding: utf-8
require 'spec_helper'

module Refinery
  describe 'frontend' do
    let(:home_page)  { FactoryGirl.create(:home_page) }
    let(:about_page) { FactoryGirl.create(:about_page) }

    before do
      # Stub the menu pages we're expecting
      Page.stub(:fast_menu).and_return([home_page, about_page])
      visit "/"
    end

    describe "about page" do
      before do
        click_link about_page.title
      end

      it "should display" do
        # in #header
        should_have_selector_within("#title-bar", "#header")
        should_have_selector_within("#breadcrumb", "#title-bar")

        # in #body
        should_have_selector_within("#who-we-are", "main")
        should_have_selector_within("#meet-the-team", "main")
      end
      
      it "should show links in #breadcrumbs" do
        within("#breadcrumb") do
          page.should have_link(home_page.title)
          page.should have_content(about_page.title)
          click_link home_page.title
        end
        should_not_have_selector_within("#title-bar", "#header")          
      end
    end
  end
end