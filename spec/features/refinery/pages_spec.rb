# encoding: utf-8
require 'spec_helper'

module Refinery
  describe 'frontend' do
    let(:home_page)  { FactoryGirl.create(:home_page) }
    let(:about_page) { FactoryGirl.create(:about_page) }

    before do
      # Stub the menu pages we're expecting
      Page.stub(:fast_menu).and_return([home_page, about_page])
      visit "/"
    end

    def standard_page_menu_items_exist?
      within("ul#main-nav-bar") do
        page.should have_content(home_page.title)
        page.should have_content(about_page.title)
      end
    end

    describe "generic page" do

      it "should display" do
        # in #body
        should_have_selector_within("div.container", "body")
        should_not_have_selector_within("#header", "body > div.container")
        should_have_selector("body > #header")

        # in #header
        should_not_have_selector("#header.row")
        should_have_selector_within("nav#top-nav-bar", "#header")
        should_have_selector_within("div#bottom-nav-bar-container", "#header")
        should_have_selector_within("div.navbar-header", "div#bottom-nav-bar-container")
        should_have_selector_within("ul#main-nav-bar", "div#bottom-nav-bar-container")

        # in #top-nav-bar
        should_not_have_selector_within("li#search-bar", "nav#top-nav-bar")
        should_have_selector_within("li#link-to-cart", "nav#top-nav-bar")

        # in #content
        should_have_selector("main#content")

        # in #footer
        should_have_selector("body > #footer")
        should_have_selector_within("#footer-top", "#footer")
        should_have_selector_within("#footer-bottom", "#footer")

        # in #footer-top
        should_have_selector_within("#footer-logo", "#footer-top")
        should_have_selector_within("#footer-address", "#footer-top")
        should_have_selector_within("#footer-stay-in-touch", "#footer-top")
      end

      context "#header" do
        it "should diplay the main menu with its item" do
          standard_page_menu_items_exist?
        end

        it "should have a search bar in #bottom-nav-bar-container", :js => true do
          should_have_selector_within("form#search-bar", "#bottom-nav-bar-container")
        end
      end

    end
  end
end