# encoding: utf-8
require 'spec_helper'

module Refinery
  describe 'frontend' do
    let(:home_page)  { FactoryGirl.create(:home_page) }
    let(:about_page) { FactoryGirl.create(:about_page) }

    before do
      # Stub the menu pages we're expecting
      Page.stub(:fast_menu).and_return([home_page, about_page])
      visit "/"
    end

    describe "homepage" do

      it "should display" do
        # in #header
        should_not_have_selector_within("#title-bar", "#header")

        # in #body
        should_have_selector("body > #carousel")
        should_have_selector_within("#leading-features", "main")
        should_have_selector_within("#featurettes", "main")
      end

    end
  end
end