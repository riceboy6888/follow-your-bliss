FactoryGirl.define do
  factory :page, :class => Refinery::Page do
    title Faker::Lorem.word

    factory :home_page do
      title "Home"
      link_url "/"
      view_template "home"
    end

    factory :about_page do
      title "About"
      view_template "about"
    end

    factory :blog_page do
      title "Blog"
      link_url "/blog"
    end
  end
end