module MySupportHelpers
  def should_have_selector_within(inner_selector, outer_selector)
    within(outer_selector) do
      page.should have_selector(inner_selector), "Within #{outer_selector}, #{inner_selector} was not found. Just add it."
    end
  end

  def should_not_have_selector_within(inner_selector, outer_selector)
    within(outer_selector) do
      page.should_not have_selector(inner_selector), "Within #{outer_selector}, #{inner_selector} was found. Just remove it."
    end
  end

  def should_have_selector(selector)
    page.should have_selector(selector), "#{selector} was not found. Just add it."
  end

  def should_not_have_selector(selector)
    page.should_not have_selector(selector), "#{selector} was found. Just remove it."
  end

end