require "spec_helper"

describe ApplicationHelper do
  describe "#main_menu" do
    it "should call refinery_menu" do
      helper.should_receive(:refinery_menu)
      helper.should_receive(:refinery_menu_pages)
      helper.main_menu
    end

    it "returns a Refinery::Pages::MenuPresenter object" do
      helper.stub(:refinery_menu)
      helper.stub(:refinery_menu_pages)
      expect(helper.main_menu).to be_an_instance_of(Refinery::Pages::MenuPresenter)
    end
  end

  describe "#render_social_network_items" do
    it "should call render_social_network_items" do
      helper.should_receive(:render_social_network_item).with("facebook")
      helper.should_receive(:render_social_network_item).with("twitter")
      helper.render_social_network_items(["facebook", "twitter"])
    end

    it "returns a list of items" do
      fb_return = %Q{<li><a href="https://www.facebook.com/manifestyourbliss" title="Follow us on Facebook"><i class="social-icon social-icon-facebook"></i></a></li>}
      tw_return = %Q{<li><a href="https://twitter.com/ManifestYrBliss" title="Follow us on Twitter"><i class="social-icon social-icon-twitter"></i></a></li>}

      helper.stub(:render_social_network_item).with("facebook").and_return(fb_return)
      helper.stub(:render_social_network_item).with("twitter").and_return(tw_return)

      expect(helper.render_social_network_items(["facebook", "twitter"])).to include(%Q{https://www.facebook.com/manifestyourbliss})
      expect(helper.render_social_network_items(["facebook", "twitter"])).to include(%Q{https://twitter.com/ManifestYrBliss})
      helper.render_social_network_items(["facebook", "twitter"]).should_not include(%Q{https://www.pinterest.com})
    end
  end

  describe "#render_social_network_item" do
    it "should call render_social_network_item" do
      helper.render_social_network_item("facebook")
    end

    it "returns an item" do
      expect(helper.render_social_network_item("facebook")).to eq(%Q{<li><a href="https://www.facebook.com/manifestyourbliss" target="_blank" title="Follow us on Facebook"><i class="social-icon social-icon-facebook"></i></a></li>})
    end
  end

  describe "#render_link_title" do
    it { helper.render_link_title }
    it { expect(helper.render_link_title(t("page_home"))).to eq(t("go_to_page", :page_name => t("page_home"))) }
    it { expect(helper.render_link_title).to eq(t("undefined")) }
  end

  describe "#render_social_links" do
    it { helper.render_social_links }
    it { expect(helper.render_social_links([["https://www.facebook.com", "facebook"]])).to eq(%Q{<li><a href="https://www.facebook.com" class="light-bg" target="_blank"><i class="social-icon social-icon-facebook"></i></a></li>}) }
    it { expect(helper.render_social_links).to eq("") }
  end
end