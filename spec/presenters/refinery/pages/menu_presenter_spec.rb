require "spec_helper"

module Refinery
  module Pages
    describe MenuPresenter do

      let(:menu_presenter) do
        menu_items = Refinery::Menu.new(FactoryGirl.create(:page))
        MenuPresenter.new(menu_items, view)
      end

      describe "config" do
        it "has default values" do
          expect(menu_presenter).to respond_to(:list_tag_id)
          expect(menu_presenter).to respond_to(:list_tag_css)
          expect(menu_presenter).to respond_to(:menu_logo)

          expect(menu_presenter.list_tag_id ).to eq("" || :list_tag_id)
          expect(menu_presenter.list_tag_css).to eq("" || :list_tag_css)
          expect(menu_presenter.menu_logo   ).to eq("" || :menu_logo)
        end
      end

      it "should render the menu in the bootstrap container" do
        expect(menu_presenter.to_html).to match(/<div class="navbar-header"><button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">/)
        expect(menu_presenter.to_html).to match(/<div class="navbar navbar-default navbar-static-top" id="bottom-nav-bar-container" role="navigation"><div class="container">/)
      end

    end
  end
end

