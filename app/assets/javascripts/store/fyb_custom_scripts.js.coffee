//= require bootstrap
//= require picturefill

# Global function that pulls #sidebar to the right and #content to the left (Blog)
window.formatBlogLayout = ->
  $("#content").addClass("pull-left")
  $("#sidebar").addClass("pull-right")

# Global function that add classes to a selector in a specific section
window.addClassToElement = (section_id, selector_name, class_names) ->
  $(section_id).find(selector_name).addClass(class_names)

# Display search bar in #main-nav-bar
display_search_bar = ->
  $("#search-bar").appendTo($("#main-nav-bar").parent())
  $("#search-bar").removeClass("hidden")

# Function that applies a simultaneous focus state on an input and its submit button
input_submit_focus = (form_id) ->
  $input = $(form_id).find(".form-control")
  $button = $(form_id).find(".btn-submit")
  $focus_class = "btn-submit-focus"

  $input.focus(->
    $button.addClass($focus_class)
  ).blur(->
    $button.removeClass($focus_class)
  )

$(document).ready ->
  init = ->
    display_search_bar()
    # Init focus effect on 'input/submit' bar
    input_submit_focus("#search-bar")
    input_submit_focus("#newsletter-bar")

  init()