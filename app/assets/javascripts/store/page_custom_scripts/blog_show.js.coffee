$(document).ready ->
  window.formatBlogLayout()
  window.addClassToElement("#main-nav-bar", "li:contains('Blog')", "active")
  window.addClassToElement("#blog_post", "img", "img-responsive")
  
  # Display content
  $("#wrapper").fadeIn()