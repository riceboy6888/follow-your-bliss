format_archive_links = ->
  $("#blog_archive_widget").find("li a").each ->
    $formatted_content = $(this).text().replace("(", "<span class='category-count pull-right'>").replace(")", "</span>")
    $(this).html($formatted_content)

$(document).ready ->
  format_archive_links()