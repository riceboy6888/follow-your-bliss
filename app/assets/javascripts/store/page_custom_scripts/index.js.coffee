# Function that format #featurettes, move the image and text to the related div
formatFeaturettes = ->
  $("#featurettes").children(".featurette").each ->
    $image_div = $(this).find(".featurette-img")
    $text_div = $(this).find(".featurette-text")

    $(this).find("img").appendTo($image_div)
    $(this).find("p:empty").remove()
    $(this).find("h2").appendTo($text_div)
    $(this).find("p").appendTo($text_div)

$(document).ready ->
  window.addClassToElement("#leading-features", "img", "img-circle")
  window.addClassToElement("#featurettes",      "img", "img-responsive")
  formatFeaturettes()

  # Display content
  $("#wrapper").fadeIn()