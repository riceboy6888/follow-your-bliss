$(document).ready ->
  window.addClassToElement("#main-nav-bar", "li:contains('Contact')", "active")
  window.addClassToElement(".show", "img", "img-responsive")

  # Display content
  $("#wrapper").fadeIn()