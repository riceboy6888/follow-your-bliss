gmap_init = ->
  # Create a map object, and include the MapTypeId to add
  # to the map type control.
  mapOptions = {
    center: new google.maps.LatLng(48.888521,2.377915)
    zoom: 16
    scrollwheel: false
    draggable: true
    disableDoubleClickZoom: 1
    mapTypeControlOptions: { mapTypeIds: [ google.maps.MapTypeId.ROADMAP, 'map_style' ] }
  }

  map = new google.maps.Map(document.getElementById("google-map"), mapOptions)
  marker = new google.maps.Marker({
    position: mapOptions.center
    map: map
    title: "We are here"
    icon: "/assets/store/google-maps-marker.png"
  })

$(document).ready ->
  window.formatBlogLayout()

  # Init Google Map
  gmap_init() if typeof google is "object" and typeof google.maps is "object"

  # Display content
  $("#wrapper").fadeIn()