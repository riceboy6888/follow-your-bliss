$(document).ready ->
  window.formatBlogLayout()
  window.addClassToElement("#main-nav-bar", "li:contains('Blog')", "active")

  window.addClassToElement("#blog_posts_list", "img", "img-responsive")
  window.addClassToElement("#blog_posts_list", "footer p", "text-center")
  window.addClassToElement("#blog_posts_list", "footer p a", "btn btn-primary btn-sm")
  
  window.addClassToElement("#blog_posts_list", ".pagination .previous_page", "icon-decima icon-decima-small-arrow-left")
  window.addClassToElement("#blog_posts_list", ".pagination .next_page", "icon-decima icon-decima-small-arrow-right")
  
  # Display content
  $("#wrapper").fadeIn()