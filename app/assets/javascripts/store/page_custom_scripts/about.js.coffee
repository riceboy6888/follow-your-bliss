# Function that formats #meet-the-team
formatMeetTheTeam = ->
  $mtt = $("#meet-the-team")
  $mtt.find(".biography img").parent().addClass("text-center")
  $mtt.find("h2").addClass("text-center")

# Function that formats the overlay size to match the biography image (#meet-the-team)
formatBiographyImgOverlay = ->
  $mtt = $("#meet-the-team")
  $img_selector = $mtt.find(".biography img")
  $overlay = $mtt.find(".biography .biography-img-overlay")

  # Set the size of the overlay to match the image size
  $overlay.css {
    "width": $img_selector.first().width()+"px"
    "height": $img_selector.first().height()+"px"
  }

  # Set the line-height of the list to match the image height
  $overlay.find("ul").css {
    "line-height": $img_selector.first().height()+"px"
  }

  # Move each overlay after the biography image
  $overlay.each ->
    $(this).appendTo($(this).parent().find("p:first"))

  # Create hover event on img + overlay container
  mouse_in = ->
    $(this).find(".biography-img-overlay").fadeIn("fast")
  mouse_out = ->
    $(this).find(".biography-img-overlay").fadeOut('fast')
  $overlay.parent().hover(mouse_in, mouse_out)

# Function that moves the social links list under the biogrpahy title (#meet-the-team)
formatBiographySocialLinks = ->
  $mtt = $("#meet-the-team")
  $bio_sl = $mtt.find(".biography-social-links")

  # Move each biography social links after the team title
  $bio_sl.each ->
    $(this).appendTo($(this).parent().find("h2"))

$(document).ready ->
  # Format #meet-the-team
  formatMeetTheTeam()

  # Format image overlay
  formatBiographyImgOverlay()

  # Format biography social links
  formatBiographySocialLinks()

  # Display content
  $("#wrapper").fadeIn()