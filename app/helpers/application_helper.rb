# ------------------------------------------------------------------------------------------------------------------------
# Application Helper
#
# Author: Pascal Huynh - Beyowi
# Created: July 2014
#
# Version 1.0 - July 2014 - Pascal Huynh - Beyowi:
# - Add main_menu, adapt it to match Twitter Bootstrap Menu
# ------------------------------------------------------------------------------------------------------------------------

module ApplicationHelper

  # Create the main menu with its styling
  def main_menu(logo="")
    menu_title = "main_menu"

    # Set the menu presenter to refinery_menu if a refinerycms-menus is created (menu_title), refinery_menu_pages otherwise (default RefineryCMS menu)
    presenter = Refinery::Pages::MenuPresenter.new((refinery_menu(menu_title).to_s.length > 0 ? refinery_menu(menu_title) : refinery_menu_pages), self)

    # Customize presenter attributes
    presenter.dom_id = ""
    presenter.css = "navbar-collapse collapse"
    presenter.list_tag_id = "main-nav-bar"
    presenter.list_tag_css = "nav navbar-nav"
    presenter.selected_css = "active"
    presenter.first_css = ""
    presenter.last_css = ""
    presenter.menu_logo = logo
    presenter
  end

  # Render social network items
  def render_social_network_items(snl=[])
    buffer = ActiveSupport::SafeBuffer.new
    snl.each do |sn| buffer << render_social_network_item(sn) end
    buffer
  end

  # Render social network item (link and icon)
  def render_social_network_item(sn="")
    content_tag :li do
      link_to(t("link_to_#{sn}"), :target => "_blank", :title => t("follow_us_on", :social_network => sn.humanize)) do
        content_tag :i, "", :class => "social-icon social-icon-#{sn}"
      end
    end
  end

  # Render a formatted link title name
  def render_link_title(page_name="")
    page_name != "" ? t("go_to_page", :page_name => page_name) : t("undefined")
  end

  # Render a formatter social link list input
  def render_social_links(social_links=[])
    buffer = ActiveSupport::SafeBuffer.new
    social_links.each do |sl|
      buffer << content_tag(:li) do
        link_to(sl[0], :class => "light-bg", :target => "_blank") do
          content_tag(:i, "", :class => "social-icon social-icon-#{sl[1]}")
        end
      end
    end
    buffer
  end

  # Display bottom left blog post link
  def render_left_blog_link(title="")
    content_tag(:span, class: "icon-decima icon-decima-small-arrow-left") do
      content_tag :span, title, class: "sr-only"
    end +
    h(truncate(pad(title)))
  end

  # Display bottom right blog post link
  def render_right_blog_link(title="")
    h(truncate(pad(title))) +
    content_tag(:span, class: "icon-decima icon-decima-small-arrow-right") do
      content_tag :span, title, class: "sr-only"
    end
  end

  # Add a space before and after a title
  def pad(title)
    " " + title + " "
  end

  # Display a close button in alert box
  def displayCloseButton
    link_to "&times;".html_safe, "#",class: "close", data: { dismiss: "alert" }
  end

  # Add selected class to the current tag name on blog 'tags' page
  def addTagExtraClass(tag_name = "")
    "selected" if defined?(@tag_name) && @tag_name == tag_name
  end

end
