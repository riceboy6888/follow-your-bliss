Spree::Role.class_eval do

    validates :name, :uniqueness => true

    def self.[](name)
      find_or_create_by_name(name.to_s)
    end

end