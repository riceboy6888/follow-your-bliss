Spree::User.class_eval do

    has_and_belongs_to_many :roles, :join_table => :spree_roles_users
    has_many :plugins, :class_name => ::Spree::RefineryUserPlugin, :order => "position ASC", :dependent => :destroy

    # add alias for refinery username (needed for refinerycms - blog)
    alias_attribute :username, :login

    def plugins=(plugin_names)
      return unless persisted?

      plugin_names = plugin_names.dup
      plugin_names.reject! { |plugin_name| !plugin_name.is_a?(String) }

      if plugins.empty?
        plugin_names.each_with_index do |plugin_name, index|
          plugins.create(:name => plugin_name, :position => index)
        end
      else
        assigned_plugins = plugins.all
        assigned_plugins.each do |assigned_plugin|
          if plugin_names.include?(assigned_plugin.name)
            plugin_names.delete(assigned_plugin.name)
          else
            assigned_plugin.destroy
          end
        end

        plugin_names.each do |plugin_name|
          plugins.create(:name => plugin_name,
                         :position => plugins.select(:position).map{|p| p.position.to_i}.max + 1)
        end
      end
    end

    def authorized_plugins
      plugins.collect(&:name) | ::Refinery::Plugins.always_allowed.names
    end

    def can_delete?(user_to_delete = self)
      user_to_delete.persisted? &&
        !user_to_delete.has_role?(:superuser) &&
        ::Spree::Role[:refinery].users.any? &&
        id != user_to_delete.id
    end

    def can_edit?(user_to_edit = self)
      user_to_edit.persisted? && (user_to_edit == self || self.has_role?(:superuser))
    end

    def add_role(name)
      raise ArgumentException, "Role should be the name of the role not a role object." if name.is_a?(::Spree::Role)
      roles << ::Spree::Role[name] unless has_role?(name)
    end

    def has_role?(name)
      raise ArgumentException, "Role should be the name of the role not a role object." if name.is_a?(::Spree::Role)
      roles.any?{|r| r.name == name.to_s}
    end

    def create_first
      if valid?
        # first we need to save user
        save
        # add refinery role
        add_role(:refinery)
        # add superuser role if there are no other users
        add_role(:superuser) if ::Spree::Role[:refinery].users.count == 1
        # add plugins
        self.plugins = Refinery::Plugins.registered.in_menu.names
      end

      # return true/false based on validations
      valid?
    end

end