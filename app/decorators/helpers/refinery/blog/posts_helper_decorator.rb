Refinery::Blog::PostsHelper.module_eval do
  def blog_archive_widget(dates=blog_archive_dates)
    ArchiveWidget.new(dates, self).display
  end
  
  class ArchiveWidget
    delegate :t, :link_to, :refinery, :render, :to => :view_context
    attr_reader :view_context

    def initialize(dates, view_context, cutoff=3.years.ago.end_of_year)
      @recent_dates, @old_dates = dates.sort_by {|date| -date.to_i }.
        partition {|date| date > cutoff }

      @view_context = view_context
    end

    def recent_links
      @recent_dates.group_by {|date| [date.year, date.month] }.
        map {|(year, month), dates| recent_link(year, month, dates.count) }
    end

    def recent_link(year, month, count)
      link_to "#{t("date.month_names")[month]} #{year} (#{count})",
        refinery.blog_archive_posts_path(:year => year, :month => month),
        class: addArchiveExtraClass(year, month)
    end

    def old_links
      @old_dates.group_by {|date| date.year }.
        map {|year, dates| old_link(year, dates.size) }
    end

    def old_link(year, count)
      link_to "#{year} (#{count})", refinery.blog_archive_posts_path(:year => year)
    end

    def links
      recent_links + old_links
    end

    def display
      return "" if links.empty?
      render "refinery/blog/widgets/blog_archive", :links => links
    end

    # Add selected class to the current archive date on blog 'archives' page
    def addArchiveExtraClass(year, month)
       "selected" if @view_context.params["year"] == year.to_s && @view_context.params["month"] == month.to_s
    end
  end
end