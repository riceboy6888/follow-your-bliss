Spree::BaseHelper.module_eval do

  def logo()
    link_to Spree::Config[:site_name], spree.root_path, :class => "logo"
  end

end