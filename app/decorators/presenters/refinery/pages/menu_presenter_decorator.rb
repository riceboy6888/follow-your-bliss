# ------------------------------------------------------------------------------------------------------------------------
# Menu Presenter Decorator
# Source: https://github.com/riceboy6888/refinerycms/blob/2-0-spree/pages/app/presenters/refinery/pages/menu_presenter.rb
#
# Author: Pascal Huynh - Beyowi
# Created: July 2014
#
# Version 1.0 - July 2014 - Pascal Huynh - Beyowi:
# - Display dom_id and css on menu tag if not blank
# - Add list_tag_id and list_tag_css
# - Add Twitter Bootstrap 3 styling
# - Add menu logo
# ------------------------------------------------------------------------------------------------------------------------

Refinery::Pages::MenuPresenter.class_eval do
  config_accessor :list_tag_id, :list_tag_css, :menu_logo

  self.list_tag_id  = "" || :list_tag_id
  self.list_tag_css = "" || :list_tag_css
  self.menu_logo    = "" || :menu_logo

  def to_html
    render_menu_in_bootstrap_container(roots) if roots.present?
  end

  private
  def render_menu_in_bootstrap_container(items)
    bottom_nav_bar_container_id = "bottom-nav-bar-container"
    bottom_nav_bar_container_css = "navbar navbar-default navbar-static-top"
    container_class = "container"

    content_tag(:div, :id => bottom_nav_bar_container_id, :class => bottom_nav_bar_container_css, :role => "navigation") do
      content_tag(:div, :class => container_class) do
        buffer = ActiveSupport::SafeBuffer.new
        buffer << render_navbar_header()
        buffer << render_menu(items)
      end
    end
  end

  def render_navbar_header
    buffer = ActiveSupport::SafeBuffer.new

    content_tag(:div, :class => "navbar-header") do
      buffer << content_tag(:button, render_collapse_button(), :type => "button", :class => "navbar-toggle", :data => { :toggle => "collapse", :target => ".navbar-collapse" })
      # Add logo if any
      if menu_logo.blank?
        buffer << content_tag(:a, "Project name", :href => "#", :class => "navbar-brand")
      else
        buffer << content_tag(:figure, menu_logo, :class => "navbar-brand")
      end
    end
  end

  def render_collapse_button
    buffer = ActiveSupport::SafeBuffer.new

    buffer << content_tag(:span, "Toggle navigation", :class => "sr-only")
    buffer << content_tag(:span, "", :class => "icon-bar")
    buffer << content_tag(:span, "", :class => "icon-bar")
    buffer << content_tag(:span, "", :class => "icon-bar")
  end

  def render_menu(items)
    content_tag(menu_tag, :id => (dom_id if !dom_id.blank?), :class => (css if !css.blank?)) do
      render_menu_items(items)
    end
  end

  def render_menu_items(menu_items)
    if menu_items.present?
      content_tag(list_tag, :id => (list_tag_id if !list_tag_id.blank?), :class => (list_tag_css if !list_tag_css.blank?)) do
        menu_items.each_with_index.inject(ActiveSupport::SafeBuffer.new) do |buffer, (item, index)|
          buffer << render_menu_item(item, index)
        end
      end
    end
  end

end