Refinery::Blog.configure do |config|
  # config.validate_source_url = false

  config.comments_per_page = 2

  config.posts_per_page = 1

  config.post_teaser_length = 500

  # config.share_this_key = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"

  # config.page_url = "/blog"
  
  # If you're grafting onto an existing app, change this to your User class
  Refinery::Blog.user_class = "Spree::User"
end
