require "bundler/capistrano"

##########################
#
# Deploy step 101
# - As Root on the server
#   adduser spree --ingroup sudo
# - Follow Key-based authentication on http://guides.spreecommerce.com/developer/manual-ubuntu.html
# - In config/recipes/lang, set the right locale
# - cap ssh:install
#   check if cap timeout: http://railscasts.com/episodes/337-capistrano-recipes?view=comments
# - cap deploy:install
#   If SSH certificate needed (Spree): https://www.digitalocean.com/community/tutorials/how-to-create-a-ssl-certificate-on-nginx-for-ubuntu-12-04
# - cap deploy:setup
#   check User credentials on MySQL
# - cap deploy:cold
# - cap deploy
# Optional- cap deploy:seed (if first time && spree do it manually: cd /home/spree/apps/followyourbliss/current; rake db:seed RAILS_ENV=production)
# Optional- if mail recipe, transfert figaro credentials
# Optional to update the sitemap - if sitemap recipe, run either sitemap:create or sitemap:refresh
# Optional if spree && refinery create refinery user credentials: https://github.com/ngn33r/spreef (rails console production)
#
##########################

load "config/recipes/base"
load "config/recipes/ssh"
load "config/recipes/nginx"
load "config/recipes/unicorn"
load "config/recipes/mysql"
load "config/recipes/nodejs"
load "config/recipes/rbenv"
load "config/recipes/lang"
load "config/recipes/rails"
load "config/recipes/imagemagick"
load "config/recipes/images"
load "config/recipes/check"
load "config/recipes/seed"
#load "config/recipes/mail"
##########################
# Bug Fix to have a pid for MySQL
# If MySQL is used, add this line in /etc/mysql/my.cnf:
# pid-file = /var/run/mysqld/mysqld.pid in the [mysqld] part
# Reference http://ubuntuforums.org/showthread.php?t=1570471
# Now MySQL can be monit
##########################
# load "config/recipes/monit"
#load "config/recipes/sitemap"

# Test server
server "37.139.12.93", :web, :app, :db, primary: true

set :application, "fyb"
set :user, "spree"
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, "git"
set :repository, "git@bitbucket.org:riceboy6888/en_fyb.git"
set :branch, "master"

set :maintenance_template_path, File.expand_path("../recipes/templates/maintenance.html.erb", __FILE__)

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

after "deploy", "deploy:cleanup" # keep only the last 5 releases