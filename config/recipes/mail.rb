namespace :mail do
  desc "Symlink the application.yml file into latest release"
  task :symlink, roles: :app do
    run "touch #{shared_path}/config/application.yml"
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
  end
  after "deploy:finalize_update", "mail:symlink"
end