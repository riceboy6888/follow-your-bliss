set_default(:sqlite3_database) { "db/production.sqlite3" }

namespace :sqlite3 do
  desc "Install the latest stable release of SQLite3."
  task :install, roles: :db, only: {primary: true} do
    run "#{sudo} apt-get -y update"
    run "#{sudo} apt-get -y install sqlite3 libsqlite3-dev"
  end
  after "deploy:install", "sqlite3:install"
    
  desc "Generate the database.yml configuration file."
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    template "sqlite3.yml.erb", "#{shared_path}/config/database.yml"
  end
  after "deploy:setup", "sqlite3:setup"
    
  desc "Symlink the database.yml file into latest release"
  task :symlink, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
  after "deploy:finalize_update", "sqlite3:symlink"
end