set_default(:mysql_host, "localhost")
set_default(:mysql_root) { "root" }
set_default(:mysql_user) { application }
set_default(:mysql_root_password) { Capistrano::CLI.password_prompt "MySQL Root Password: " }
set_default(:mysql_user_password) { Capistrano::CLI.password_prompt "MySQL User Password: " }
set_default(:mysql_database) { "#{application}_production" }

namespace :mysql do
  desc "Install the latest stable release of MySQL."
  task :install, roles: :db, only: {primary: true} do
    run "#{sudo} apt-get -y install mysql-server mysql-client libmysqlclient-dev" do |channel, stream, data|
      # prompts for mysql root password (when blue screen appears)
      channel.send_data("#{mysql_root_password}\n\r") if data =~ /password/
    end
  end
  after "deploy:install", "mysql:install"
    
  desc "Create a database for this application."
  task :create_database, roles: :db, only: {primary: true} do
    run %Q{#{sudo} mysql -u root -p#{mysql_root_password} -e "CREATE USER #{mysql_user}@#{mysql_host} IDENTIFIED BY '#{mysql_user_password}';"}
    run %Q{#{sudo} mysql -u root -p#{mysql_root_password} -e "create database #{mysql_database};"}
    # Bug - The grant privilege doesn't seem to work right need to do it manually
    run %Q{#{sudo} mysql -u root -p#{mysql_root_password} -e "grant all privileges on #{mysql_database}.* to #{mysql_user}@#{mysql_host} identified by '#{mysql_user_password}';"}
    run %Q{#{sudo} mysql -u root -p#{mysql_root_password} -e "flush privileges;"}
  end
  after "deploy:setup", "mysql:create_database"
    
  desc "Generate the database.yml configuration file."
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    template "mysql.yml.erb", "#{shared_path}/config/database.yml"
  end
  after "deploy:setup", "mysql:setup"
    
  desc "Symlink the database.yml file into latest release"
  task :symlink, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
  after "deploy:finalize_update", "mysql:symlink"
end