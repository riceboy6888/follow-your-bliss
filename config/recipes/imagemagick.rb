namespace :imagemagick do
  desc "Install the latest stable release of Image Magick."
  task :install, roles: :app do
    run "#{sudo} apt-get -y install imagemagick"
  end
  after "deploy:install", "imagemagick:install"
end