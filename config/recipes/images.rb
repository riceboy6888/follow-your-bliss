namespace :images do
  desc "Create spree directory in shared folder."
  task :create, roles: :app do
    run "mkdir -p #{shared_path}/spree"
  end
  after "deploy:setup", "images:create"

  task :symlink, :except => { :no_release => true } do
    run "rm -rf #{release_path}/public/spree"
    run "ln -nfs #{shared_path}/spree #{release_path}/public/spree"
  end
  after "deploy", "images:symlink"
end