# Value to set the locale
# en_US.utf8, en_US
# fr_FR.utf8, fr_FR

set_default :lang_encoding, "en_US.utf8"
set_default :lang_language, "en_US"

namespace :lang do
  desc "Set encoding language"
  task :install, roles: :app do
    run "#{sudo} locale-gen #{lang_language}"
    run "#{sudo} locale-gen #{lang_encoding}"
    run "#{sudo} dpkg-reconfigure locales"

    bashrc = <<-BASHRC
export LANGUAGE=#{lang_language}
export LC_CTYPE=#{lang_encoding}
export LANG=#{lang_encoding}
unset LC_ALL
BASHRC
    put bashrc, "/tmp/enclang"
    run "cat /tmp/enclang ~/.bashrc > ~/.bashrc.tmp"
    run "mv ~/.bashrc.tmp ~/.bashrc"
  end
  after "deploy:install", "lang:install"
end