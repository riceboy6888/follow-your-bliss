set_default(:rails_version, "3.2.17")

namespace :rails do
  desc "Install the latest stable release of Rails."
  task :install, roles: :app do
    run "#{sudo} /home/#{user}/.rbenv/shims/gem install rails -v #{rails_version}"
  end
  after "deploy:install", "rails:install"
end