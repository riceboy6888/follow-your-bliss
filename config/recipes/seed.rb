namespace :deploy do
  desc "Load the database with seed data"
  task :seed, roles: :web do
    run "cd #{current_path}; rake db:seed RAILS_ENV=#{rails_env}"
  end

  desc "Reset the database"
  task :reset_db, roles: :web do
    run "cd #{current_path}; rake db:drop RAILS_ENV=#{rails_env}"
    run "cd #{current_path}; rake db:create RAILS_ENV=#{rails_env}"
    run "cd #{current_path}; rake db:migrate RAILS_ENV=#{rails_env}"
  end
end