namespace :sitemap do
  desc "Clean previous sitemaps"
  task :clean, roles: :web do
    run "cd #{current_path}; cd public; rm -rf sitemap.*"
  end

  desc "Create sitemap without pinging search engines"
  task :create, roles: :web do
    run "cd #{current_path}; rake sitemap:refresh:no_ping RAILS_ENV=#{rails_env}"
  end

  desc "Create sitemaps and ping search engine"
  task :refresh, roles: :web do
    run "cd #{current_path}; rake sitemap:refresh RAILS_ENV=#{rails_env}"
  end
  
  desc "Unzip sitemap"
  task :unzip, roles: :web do
    run "cd #{current_path}; cd public; gunzip -c sitemap.xml.gz > sitemap.xml"
  end
  
  before "sitemap:create",  "sitemap:clean"
  before "sitemap:refresh", "sitemap:clean"
  after "sitemap:create",   "sitemap:unzip"
  after "sitemap:refresh",  "sitemap:unzip"
end